# LogoPlus

Un plugin pour tester l'édition des logos permise par SPIP 4+.

## Contexte

Depuis SPIP 4, les logos sont stockés dans la table spip_documents.

Mais l'interface d'administration ne donne pas accès aux logos de la même manière que les autres documents.

Ce plugin est un bac à sable pour proposer une façon de le faire dans l'admin et d'avoir accès aux informations supplémentaires depuis le public à l'aide de nouvelles balises.

## Fonctionnement

1. Activer le plugin
2. Dans l'admin, le titre des logos est affiché (et éditable si les Crayons sont installés et configurés pour permettre l'édition dans les pages d'admin) et un bouton modifier est disponible pour aller éditer le logo (bonus : permet de modifier un logo normal sans avoir à supprimer le logo de survol)
3. Côté public, vous disposez de nouvelles balises

## Nouvelles balises #LOGO_

4 nouveaux suffixes sont disponibles sur les balises #LOGO_ (cf https://www.spip.net/fr_article6461.html) :
* `_TITRE` : pour afficher le titre du logo
* `_DESCRIPTIF`
* `_CREDITS`
* `_ID` : pour récupérer l'id du documents du logo

## Objectif à terme

L'objectif de ce plugin est qu'il soit intégré à SPIP 4.2