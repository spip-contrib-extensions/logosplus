<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-savecfg
// Langue: fr
// Date: 04-06-2015 16:40:09
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'logosplus_description' => 'LogoPlus donne accès à la page de modification d’un logo et permet d’utiliser les balises de logos avec _TITRE, _DESCRIPTIF, _CREDITS, _ALT, _ID
	
	Exemple : #LOGO_ARTICLE_TITRE ou #LOGO_ARTICLE_RUBRIQUE_TITRE',
	'logosplus_slogan' => 'Accès à la modification des logos + balises côté public',
);
?>